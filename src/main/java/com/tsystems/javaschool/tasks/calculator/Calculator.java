package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            return evaluateExpression(statement);
        } catch (Exception e) {
            return null;
        }
    }

    private ArrayList<String> operators = new ArrayList<>(Arrays.asList("+", "-", "*", "/"));

    private String evaluateExpression(String statement) {
        Stack<Float> stack = new Stack<>();

        for (Object element : toPostfix(statement)) {
            String e = element.toString();
            if (operators.contains(e)) {
                Float n1 = stack.pop();
                Float n2 = stack.pop();

                switch (e) {
                    case "+": stack.push(n2 + n1); break;
                    case "-": stack.push(n2 - n1); break;
                    case "*": stack.push(n2 * n1); break;
                    case "/":
                        if (n1 == 0) return null;
                        stack.push(n2 / n1);
                        break;
                }
            } else {
                stack.push(Float.valueOf(e));
            }
        }

        Float result = stack.pop();
        if (result == Math.round(result)) {
            return ((Integer) Math.round(result)).toString();
        } else {
            return Float.toString(Math.round(result * 10000f) / 10000f);
        }
    }

    private ArrayList toPostfix(String statement) {
        ArrayList<String> postfix = new ArrayList<>();
        Deque<String> stack  = new LinkedList<>();
        String[] statementArray = statement
                .replaceAll("\\s+","")
                .split("(?<=[-+*/()])|(?=[-+*/()])");

        for (String token : statementArray) {
            if (operators.contains(token)) {
                while (!stack.isEmpty() && (operators.indexOf(token) <= operators.indexOf(stack.peek()))) {
                    postfix.add(stack.pop());
                }
                stack.push(token);
            } else if (token.equals("(")) {
                stack.push(token);
            } else if (token.equals(")")) {
                while (!stack.peek().equals("(")) {
                    postfix.add(stack.pop());
                }
                stack.pop();
            } else {
                postfix.add(token);
            }
        }

        while (!stack.isEmpty()) {
            postfix.add(stack.pop());
        }

        return postfix;
    }
}

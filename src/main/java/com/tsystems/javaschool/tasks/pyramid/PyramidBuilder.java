package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        double m = (Math.sqrt(8 * inputNumbers.size() + 1) - 1) / 2;
        int n = (int) m;
        if (m != n || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);

        int[][] array = new int[n][n + n - 1];

        for (int i = 0, k = 0; i < array.length; i++) {
            for (int j = n - 1, t = 0; j < array[0].length; j += 2, t++) {
                array[i][j] = inputNumbers.get(k);
                k++;
                if (i == t) break;
            }
            n--;
        }

        return array;
    }
}
